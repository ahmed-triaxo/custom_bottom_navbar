import 'package:custom_navigation_bar/navigation_bloc/navigation_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

void main() {
  runApp(TestApp());
}

class TestApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "Test App",
      home: BlocProvider<NavigationBloc>(
          create: (_) => NavigationBloc(), child: HomePage()),
    );
  }
}

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  Home? home;

  @override
  Widget build(BuildContext context) {
    print('build is rebuild--->');
    return BlocBuilder<NavigationBloc, int>(
      builder: (context, state) => Scaffold(
        body: _getPage(page: state),
        bottomNavigationBar: CustomBottomNavigationBar(
          iconList: [
            Icons.home,
            Icons.card_giftcard,
            Icons.pie_chart,
            Icons.person,
          ],
          onChange: (val) {
            if (state == val) return;
            print('-----------------$val');
            context.read<NavigationBloc>().pageChanged(index: val);
          },
        ),
      ),
    );
  }

  Widget _getPage({required int page}) {
    switch (page) {
      case 0:
        {
          if (home == null) home = const Home(key: Key('sasas'));
          return home!;
        }
      case 1:
        return Buy();
      case 2:
        return Pie();
      case 3:
        return Profile();
      default:
        return const SizedBox();
    }
  }
}

class CustomBottomNavigationBar extends StatefulWidget {
  // final Function(int) onChange;
  final Function onChange;
  final List<IconData> iconList;

  CustomBottomNavigationBar({required this.iconList, required this.onChange});

  @override
  _CustomBottomNavigationBarState createState() =>
      _CustomBottomNavigationBarState();
}

class _CustomBottomNavigationBarState extends State<CustomBottomNavigationBar> {
  int _selectedIndex = 0;
  List<IconData> _iconList = [];

  @override
  void initState() {
    super.initState();

    _iconList = widget.iconList;
    print('the length is -- > ${_iconList.length}');
  }

  @override
  Widget build(BuildContext context) {
    return Row(
        children: List.generate(_iconList.length,
            (index) => buildNavBarItem(_iconList[index], index)));
  }

  Widget buildNavBarItem(IconData icon, int index) {
    return GestureDetector(
      onTap: () {
        widget.onChange(index);
        _selectedIndex = index;
      },
      child: Container(
        height: 60,
        width: MediaQuery.of(context).size.width / _iconList.length,
        decoration: index == _selectedIndex
            ? BoxDecoration(
                border: Border(
                  bottom: BorderSide(width: 4, color: Colors.green),
                ),
                gradient: LinearGradient(colors: [
                  Colors.green.withOpacity(0.3),
                  Colors.green.withOpacity(0.015),
                ], begin: Alignment.bottomCenter, end: Alignment.topCenter))
            : BoxDecoration(),
        child: Icon(
          icon,
          color: index == _selectedIndex ? Colors.black : Colors.grey,
          size: index == _selectedIndex ? 25.0 : 20.0,
        ),
      ),
    );
  }
}

class Home extends StatelessWidget {
  const Home({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    print('home build is rebuild--->');
    return Scaffold(
      appBar: AppBar(
        title: Text('Home'),
        centerTitle: true,
        backgroundColor: Colors.green,
      ),
      body: Center(
        child: Text('home'),
      ),
    );
  }
}

class Buy extends StatelessWidget {
  const Buy({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    print('buy build is rebuild--->');
    return Scaffold(
      appBar: AppBar(
        title: Text('Buy'),
        centerTitle: true,
        backgroundColor: Colors.green,
      ),
      body: Center(
        child: Text('Buy'),
      ),
    );
  }
}

class Pie extends StatelessWidget {
  const Pie({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    print('pie build is rebuild--->');
    return Scaffold(
      appBar: AppBar(
        title: Text('Pie'),
        centerTitle: true,
        backgroundColor: Colors.green,
      ),
      body: Center(
        child: Text('Pie'),
      ),
    );
  }
}

class Profile extends StatelessWidget {
  const Profile({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    print('profile build is rebuild--->');
    return Scaffold(
      appBar: AppBar(
        title: Text('Profile'),
        centerTitle: true,
        backgroundColor: Colors.green,
      ),
      body: Center(
        child: Text('Profile'),
      ),
    );
  }
}
