import 'package:flutter_bloc/flutter_bloc.dart';

class NavigationBloc extends Cubit<int> {
  //--constructor initialization of state
  NavigationBloc() : super(0);
//--to change page on index basis
  void pageChanged({required int index}) {
    emit(index);
  }
}
